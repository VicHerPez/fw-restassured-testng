import com.github.fge.jsonschema.SchemaVersion
import com.github.fge.jsonschema.cfg.ValidationConfiguration
import com.github.fge.jsonschema.main.JsonSchemaFactory
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import io.restassured.RestAssured
import org.junit.*

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse
import static com.github.tomakehurst.wiremock.client.WireMock.configureFor
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import static io.restassured.RestAssured.get
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath
import static io.restassured.module.jsv.JsonSchemaValidatorSettings.settings
import static org.hamcrest.Matchers.equalTo
import static org.hamcrest.Matchers.hasItems

class RestAssuredIntegrationTest {
    private static WireMockServer wireMockServer

    private static final String EVENTS_PATH = "/events?id=390"
    private static final String APPLICATION_JSON = "application/json"
    private static final String GAME_ODDS = getEventJson()

    @BeforeClass
    static void before() throws Exception {
        System.out.println("Setting up!")
        final int port = Util.getAvailablePort()
        wireMockServer = new WireMockServer(port)
        wireMockServer.start()
        RestAssured.port = port
        configureFor("localhost", port)
        stubFor(WireMock.get(urlEqualTo(EVENTS_PATH)).willReturn(
                aResponse().withStatus(200)
                        .withHeader("Content-Type", APPLICATION_JSON)
                        .withBody(GAME_ODDS)))
    }

    @Test
    void givenUrl_whenCheckingFloatValuePasses_thenCorrect() {
        get("/events?id=390").then().assertThat()
                .body("odd.ck", equalTo(12.2f))
    }

    @Test
    void givenUrl_whenSuccessOnGetsResponseAndJsonHasRequiredKV_thenCorrect() {

        get("/events?id=390").then().statusCode(200).assertThat()
                .body("id", equalTo("390"))
    }

    @Test
    void givenUrl_whenJsonResponseHasArrayWithGivenValuesUnderKey_thenCorrect() {
        get("/events?id=390").then().assertThat()
                .body("odds.price", hasItems("1.30", "5.25", "2.70", "1.20"))
    }

    @Test
    void givenUrl_whenJsonResponseConformsToSchema_thenCorrect() {
        get("/events?id=390")
                .then()
                .assertThat()
                .body(matchesJsonSchemaInClasspath("event.json"))
    }

    @Test
    void givenUrl_whenValidatesResponseWithInstanceSettings_thenCorrect() {
        JsonSchemaFactory jsonSchemaFactory = JsonSchemaFactory
                .newBuilder()
                .setValidationConfiguration(
                        ValidationConfiguration.newBuilder()
                                .setDefaultVersion(SchemaVersion.DRAFTV4)
                                .freeze()).freeze()

        get("/events?id=390")
                .then()
                .assertThat()
                .body(matchesJsonSchemaInClasspath("event.json").using(
                        jsonSchemaFactory))
    }

    @Test
    void givenUrl_whenValidatesResponseWithStaticSettings_thenCorrect() {

        get("/events?id=390")
                .then()
                .assertThat()
                .body(matchesJsonSchemaInClasspath("event.json").using(
                        settings().with().checkedValidation(false)))
    }

    @AfterClass
    static void after() throws Exception {
        System.out.println("Running: tearDown")
        wireMockServer.stop()
    }

    private static String getEventJson() {
        return Util.inputStreamToString(RestAssuredIntegrationTest.class
                .getResourceAsStream("/event.json"))
    }
}